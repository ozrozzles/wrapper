import { WrapperClass } from "../../Decorators"
import Hero from "../Base/Hero"

@WrapperClass("CDOTA_Unit_Hero_Tiny")
export default class npc_dota_hero_tiny extends Hero {
}
