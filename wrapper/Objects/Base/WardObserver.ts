import { WrapperClass } from "../../Decorators"
import Unit from "./Unit"

@WrapperClass("CDOTA_NPC_Observer_Ward")
export default class WardObserver extends Unit {
	public get RingRadius(): number {
		return 64
	}
}
