import QAngle from "../Base/QAngle"
import Vector2 from "../Base/Vector2"
import Vector3 from "../Base/Vector3"
import { MaterialFlags } from "../Resources/ParseMaterial"
import { ParseREDI } from "../Resources/ParseREDI"
import { ParseResourceLayout } from "../Resources/ParseResource"
import { DegreesToRadian } from "../Utils/Math"
import readFile from "../Utils/readFile"

export class CHeightMap {
	constructor(
		private readonly MinMapCoords_: Vector2,
		private readonly MapSize_: Vector2,
	) { }

	public get MinMapCoords(): Vector2 {
		return this.MinMapCoords_.Clone()
	}

	public get MapSize(): Vector2 {
		return this.MapSize_.Clone()
	}

	public get MaxMapCoords(): Vector2 {
		return this.MinMapCoords_.Add(this.MapSize_)
	}
}
export let HeightMap: Nullable<CHeightMap>

enum HeightMapParseError {
	NONE = 0,
	INVALID_MAGIC = 1,
	UNKNOWN_VERSION = 2,
	ALLOCATION_ERROR = 3,
};

function TryLoadWASMModule(name: string): WebAssembly.Module {
	const wasm_file = readFile(name)
	if (wasm_file === undefined)
		throw `${name} not found`

	return new WebAssembly.Module(wasm_file)
}

function GetWASMModule(): WebAssembly.Module {
	try {
		return TryLoadWASMModule("wrapper_simd.wasm")
	} catch {
		return TryLoadWASMModule("wrapper.wasm")
	}
}

const WASI_ESUCCESS = 0
const WASI_ENOSYS = 52
const wasm = new WebAssembly.Instance(GetWASMModule(), {
	env: {
		emscripten_notify_memory_growth,
	},
	wasi_snapshot_preview1: {
		proc_exit: () => {
			return WASI_ENOSYS
		},
		args_get: () => {
			return WASI_ESUCCESS
		},
		args_sizes_get: (argc: number, argvBufSize: number) => {
			const view = new DataView(wasm.memory.buffer)
			view.setUint32(argc, 0)
			view.setUint32(argvBufSize, 0)
			return WASI_ESUCCESS
		},
		fd_write: () => {
			throw new Error("fd_write is unimplemented")
		},
		fd_seek: () => {
			throw new Error("fd_seek is unimplemented")
		},
		fd_close: () => {
			throw new Error("fd_close is unimplemented")
		},
	},
}).exports as any as {
	_start: () => void,
	GetIOBuffer: () => number,
	memory: WebAssembly.Memory,
	ScreenToWorld: () => void,
	WorldToScreen: () => boolean,
	ParseVHCG: (ptr: number, size: number) => HeightMapParseError,
	GetHeightForLocation: () => void,
	GetLocationAverageHeight: () => void,
	GetCursorRay: () => void,
	ScreenToWorldFar: () => void,
	my_malloc: (size: number) => number,
	my_free: (ptr: number) => void,
	ParsePNG: (data: number, size: number) => number,
	ParseVTex: (
		data: number,
		size: number,
		image_data: number,
		is_YCoCg: boolean,
		normalize: boolean,
		is_inverted: boolean,
		hemi_oct: boolean,
		hemi_oct_RB: boolean,
	) => number,
	MurmurHash2: (ptr: number, size: number, seed: number) => number,
	MurmurHash64: (ptr: number, size: number, seed: number) => void,
	CRC32: (ptr: number, size: number) => number,
	DecompressLZ4: (ptr: number, size: number, dst_len: number) => number,
	DecompressZstd: (ptr: number, size: number) => number,
	DecompressLZ4Chained: (ptr: number, input_sizes_ptr: number, output_sizes_ptr: number, count: number) => number,
	CloneWorldToProjection: () => void,
	WorldToScreenNew: () => boolean,
	DecompressVertexBuffer: (ptr: number, size: number, elem_count: number, elem_size: number) => number,
	DecompressIndexBuffer: (ptr: number, size: number, elem_count: number, elem_size: number) => number,
	ResetWorld: () => void,
	LoadWorldModel: (
		vertex_ptr: number, vertex_size: number,
		index_ptr: number, index_size: number, index_elem_size: number,
	) => void,
	FinishWorld: () => void,
	FinishWorldCached: (
		nodes_ptr: number, nodes_size: number,
		indices_ptr: number, indices_size: number,
	) => void,
	ExtractWorld: () => void,
}
declare global {
	var wasm_: typeof wasm
}
globalThis.wasm_ = wasm
// wasm._start() // calls proc_exit => unreachable code

export let WASMIOBuffer: Float32Array
export let WASMIOBufferBU64: BigUint64Array
export let WASMIOBufferU32: Uint32Array
function emscripten_notify_memory_growth(_memoryIndex: number) {
	const off = wasm.GetIOBuffer()
	WASMIOBuffer = new Float32Array(wasm.memory.buffer, off)
	WASMIOBufferBU64 = new BigUint64Array(wasm.memory.buffer, off)
	WASMIOBufferU32 = new Uint32Array(wasm.memory.buffer, off)
}
emscripten_notify_memory_growth(0)

export function GetEyeVector(camera_angles: QAngle): Vector3 {
	// TODO: should we use Math.cos(DegreesToRadian(camera_angles.y))?
	return new Vector3(0, Math.cos(DegreesToRadian(camera_angles.x) - Math.cos(DegreesToRadian(camera_angles.y))), -Math.sin(DegreesToRadian(camera_angles.x)))
}

let camera_offset = 0,
	camera_offset_updated = 0
export function GetCameraPosition(
	camera_position: Vector2,
	camera_distance: number,
	camera_angles: QAngle,
): Vector3 {
	const dist = 96 * 1.5,
		count = 8,
		t = hrtime(),
		eye_vector = GetEyeVector(camera_angles)
	if (camera_offset_updated + 100 < t) {
		camera_offset = GetLocationAverageHeight(
			camera_position
				.Clone()
				.AddScalarX(eye_vector.x * camera_distance)
				.AddScalarY(eye_vector.y * camera_distance),
			count,
			dist,
		)
		camera_offset_updated = t
	}
	return Vector3.FromVector2(camera_position).SetZ(
		camera_offset
		- eye_vector.z * camera_distance
		+ 50,
	)
}

export function GetCursorRay(
	screen: Vector2,
	window_size: Vector2,
	camera_position: Vector3,
	camera_distance: number,
	camera_angles: QAngle,
	fov: number,
): Vector3 {
	WASMIOBuffer[0] = window_size.x
	WASMIOBuffer[1] = window_size.y

	WASMIOBuffer[2] = camera_position.x
	WASMIOBuffer[3] = camera_position.y
	WASMIOBuffer[4] = camera_position.z

	WASMIOBuffer[5] = camera_angles.x
	WASMIOBuffer[6] = camera_angles.y
	WASMIOBuffer[7] = camera_angles.z

	WASMIOBuffer[8] = camera_distance

	WASMIOBuffer[9] = fov

	WASMIOBuffer[10] = screen.x
	WASMIOBuffer[11] = screen.y

	wasm.GetCursorRay()
	return new Vector3(
		WASMIOBuffer[0],
		WASMIOBuffer[1],
		WASMIOBuffer[2],
	)
}

export function ScreenToWorldFar(
	screens: Vector2[],
	window_size: Vector2,
	camera_position: Vector3,
	camera_distance: number,
	camera_angles: QAngle,
	fov: number,
	flags = MaterialFlags.Nonsolid | MaterialFlags.Water,
): Vector3[] {
	WASMIOBuffer[0] = window_size.x
	WASMIOBuffer[1] = window_size.y

	WASMIOBuffer[2] = camera_position.x
	WASMIOBuffer[3] = camera_position.y
	WASMIOBuffer[4] = camera_position.z

	WASMIOBuffer[5] = camera_angles.x
	WASMIOBuffer[6] = camera_angles.y
	WASMIOBuffer[7] = camera_angles.z

	WASMIOBuffer[8] = camera_distance

	WASMIOBuffer[9] = fov

	WASMIOBufferU32[10] = flags
	WASMIOBufferU32[11] = screens.length

	screens.forEach((screen, i) => {
		WASMIOBuffer[12 + i * 2 + 0] = screen.x
		WASMIOBuffer[12 + i * 2 + 1] = screen.y
	})

	wasm.ScreenToWorldFar()
	return screens.map((_, i) => new Vector3(
		WASMIOBuffer[i * 3 + 0],
		WASMIOBuffer[i * 3 + 1],
		WASMIOBuffer[i * 3 + 2],
	))
}

export function WorldToScreen(
	position: Vector3,
	camera_position: Vector3,
	camera_distance: number,
	camera_angles: QAngle,
	window_size: Vector2,
): Nullable<Vector2> {
	WASMIOBuffer[0] = position.x
	WASMIOBuffer[1] = position.y
	WASMIOBuffer[2] = position.z

	WASMIOBuffer[3] = camera_position.x
	WASMIOBuffer[4] = camera_position.y
	WASMIOBuffer[5] = camera_position.z

	WASMIOBuffer[6] = camera_angles.x
	WASMIOBuffer[7] = camera_angles.y
	WASMIOBuffer[8] = camera_angles.z

	WASMIOBuffer[9] = camera_distance

	WASMIOBuffer[10] = window_size.x
	WASMIOBuffer[11] = window_size.y

	if (!wasm.WorldToScreen())
		return undefined
	return new Vector2(WASMIOBuffer[0], WASMIOBuffer[1])
}

export function ScreenToWorld(
	screen: Vector2,
	camera_position: Vector3,
	camera_distance: number,
	camera_angles: QAngle,
	window_size: Vector2,
): Vector3 {
	WASMIOBuffer[0] = screen.x
	WASMIOBuffer[1] = screen.y

	WASMIOBuffer[2] = camera_position.x
	WASMIOBuffer[3] = camera_position.y
	WASMIOBuffer[4] = camera_position.z

	WASMIOBuffer[5] = camera_angles.x
	WASMIOBuffer[6] = camera_angles.y
	WASMIOBuffer[7] = camera_angles.z

	WASMIOBuffer[8] = camera_distance

	WASMIOBuffer[9] = window_size.x
	WASMIOBuffer[10] = window_size.y

	wasm.ScreenToWorld()
	return new Vector3(WASMIOBuffer[0], WASMIOBuffer[1], WASMIOBuffer[2])
}

function ParsePNG(buf: Uint8Array): [Uint8Array, Vector2] {
	let addr = wasm.my_malloc(buf.byteLength)
	new Uint8Array(wasm.memory.buffer, addr).set(buf)

	addr = wasm.ParsePNG(addr, buf.byteLength)
	if (addr === 0)
		throw "PNG Image conversion failed: WASM failure"
	const image_size = new Vector2(WASMIOBufferU32[0], WASMIOBufferU32[1])
	const copy = new Uint8Array(image_size.x * image_size.y * 4)
	copy.set(new Uint8Array(wasm.memory.buffer, addr, copy.byteLength))
	wasm.my_free(addr)

	return [copy, image_size]
}

export function ParseImage(buf: Uint8Array): [Uint8Array, Vector2] {
	if (buf.byteLength > 8 && new BigUint64Array(buf.buffer, buf.byteOffset, 8)[0] === 0x0A1A0A0D474E5089n)
		return ParsePNG(buf)

	const layout = ParseResourceLayout(buf)
	if (layout === undefined)
		throw "Image conversion failed"

	const data_block = layout[0].get("DATA")
	if (data_block === undefined)
		throw "Image conversion failed: missing DATA block"
	if (data_block.byteLength < 40)
		throw "Image conversion failed: too small file"
	// TODO: add check that's real VTEX file (lookup https://github.com/SteamDatabase/ValveResourceFormat/blob/master/ValveResourceFormat/Resource/Resource.cs)
	if (new Uint16Array(data_block.buffer, data_block.byteOffset, 2)[0] !== 1)
		throw `Image conversion failed: unknown VTex version`
	const redi_block = layout[0].get("REDI")
	let is_YCoCg = false,
		normalize = false,
		is_inverted = false,
		hemi_oct = false,
		hemi_oct_RB = false
	if (redi_block !== undefined) {
		const REDI = ParseREDI(redi_block)
		is_YCoCg = REDI.SpecialDependencies.some(dep => (
			dep.compiler_identifier === "CompileTexture"
			&& dep.str === "Texture Compiler Version Image YCoCg Conversion"
		))
		normalize = REDI.SpecialDependencies.some(dep => (
			dep.compiler_identifier === "CompileTexture"
			&& dep.str === "Texture Compiler Version Image NormalizeNormals"
		))
		is_inverted = REDI.SpecialDependencies.some(dep => (
			dep.compiler_identifier === "CompileTexture"
			&& dep.str === "Texture Compiler Version LegacySource1InvertNormals"
		))
		hemi_oct = REDI.SpecialDependencies.some(dep => (
			dep.compiler_identifier === "CompileTexture"
			&& dep.str === "Texture Compiler Version Mip HemiOctAnisoRoughness"
		))
		hemi_oct_RB = REDI.SpecialDependencies.some(dep => (
			dep.compiler_identifier === "CompileTexture"
			&& dep.str === "Texture Compiler Version Mip HemiOctIsoRoughness_RG_B"
		))
	}

	const data_size = buf.byteLength - data_block.byteOffset
	let addr = wasm.my_malloc(data_size)
	new Uint8Array(wasm.memory.buffer, addr).set(buf.subarray(data_block.byteOffset))

	addr = wasm.ParseVTex(
		addr,
		buf.byteLength,
		addr + data_block.byteLength,
		is_YCoCg,
		normalize,
		is_inverted,
		hemi_oct,
		hemi_oct_RB,
	)
	if (addr === 0)
		throw "Image conversion failed: WASM failure"
	const image_size = new Vector2(WASMIOBufferU32[0], WASMIOBufferU32[1])
	const copy = new Uint8Array(image_size.x * image_size.y * 4)
	copy.set(new Uint8Array(wasm.memory.buffer, addr, copy.byteLength))
	wasm.my_free(addr)

	return [copy, image_size]
}

export function ParseVHCG(buf: Uint8Array): void {
	try {
		const addr = wasm.my_malloc(buf.byteLength)
		if (addr === 0)
			throw "Memory allocation for VHCG raw data failed"
		const memory = new Uint8Array(wasm.memory.buffer, addr)
		memory.set(buf)

		const err = wasm.ParseVHCG(addr, buf.byteLength)
		if (err !== HeightMapParseError.NONE) {
			if (err < 0)
				throw `VHCG parse failed with READ_ERROR ${-err}`
			throw `VHCG parse failed with ${HeightMapParseError[err]}`
		}
		HeightMap = new CHeightMap(
			new Vector2(WASMIOBuffer[0], WASMIOBuffer[1]),
			new Vector2(WASMIOBuffer[2], WASMIOBuffer[3]),
		)
	} catch (e) {
		console.log("Error in HeightMap init: " + e)
		HeightMap = undefined
	}
}
export function ResetVHCG(): void {
	HeightMap = undefined
}

export function GetPositionHeight(
	loc: Vector2 | Vector3,
	flags = MaterialFlags.Nonsolid | MaterialFlags.Water,
): number {
	WASMIOBuffer[0] = loc.x
	WASMIOBuffer[1] = loc.y
	WASMIOBufferU32[2] = flags

	wasm.GetHeightForLocation()
	return WASMIOBuffer[0]
}

export function GetLocationAverageHeight(
	position: Vector2,
	count: number,
	distance: number,
	flags = MaterialFlags.Nonsolid | MaterialFlags.Water,
): number {
	WASMIOBuffer[0] = position.x
	WASMIOBuffer[1] = position.y
	WASMIOBufferU32[2] = flags
	WASMIOBuffer[3] = count
	WASMIOBuffer[4] = distance

	wasm.GetLocationAverageHeight()
	return WASMIOBuffer[0]
}

export function MurmurHash2(buf: Uint8Array, seed = 0x31415926): number {
	const buf_addr = wasm.my_malloc(buf.byteLength)
	if (buf_addr === 0)
		throw "Memory allocation for MurmurHash2 raw data failed"
	new Uint8Array(wasm.memory.buffer, buf_addr, buf.byteLength).set(buf)

	return wasm.MurmurHash2(buf_addr, buf.byteLength, seed) >>> 0
}

export function MurmurHash64(buf: Uint8Array, seed = 0xEDABCDEF): bigint {
	const buf_addr = wasm.my_malloc(buf.byteLength)
	if (buf_addr === 0)
		throw "Memory allocation for MurmurHash64 raw data failed"
	new Uint8Array(wasm.memory.buffer, buf_addr, buf.byteLength).set(buf)

	wasm.MurmurHash64(buf_addr, buf.byteLength, seed)
	return WASMIOBufferBU64[0]
}

export function CRC32(buf: Uint8Array): number {
	const buf_addr = wasm.my_malloc(buf.byteLength)
	if (buf_addr === 0)
		throw "Memory allocation for MurmurHash2 raw data failed"
	new Uint8Array(wasm.memory.buffer, buf_addr, buf.byteLength).set(buf)

	return wasm.CRC32(buf_addr, buf.byteLength) >>> 0
}

export function DecompressLZ4(buf: Uint8Array, dst_len: number): Uint8Array {
	let addr = wasm.my_malloc(buf.byteLength)
	new Uint8Array(wasm.memory.buffer, addr).set(buf)

	addr = wasm.DecompressLZ4(addr, buf.byteLength, dst_len)
	if (addr === 0)
		throw "LZ4 decompression failed"
	const copy = new Uint8Array(dst_len)
	copy.set(new Uint8Array(wasm.memory.buffer, addr, copy.byteLength))
	wasm.my_free(addr)

	return copy
}

export function DecompressZstd(buf: Uint8Array): Uint8Array {
	if (buf.byteLength < 12)
		throw `Zstd decompression failed: buf.byteLength < 12`
	const addr = wasm.my_malloc(buf.byteLength)
	new Uint8Array(wasm.memory.buffer, addr).set(buf)

	const decompressed_addr = wasm.DecompressZstd(addr, buf.byteLength)
	const size = new Uint32Array(wasm.memory.buffer, addr)[0],
		error = new Uint32Array(wasm.memory.buffer, addr)[1],
		additional_data = new Uint32Array(wasm.memory.buffer, addr)[2]
	wasm.my_free(addr)
	if (error !== 0 || decompressed_addr === 0)
		throw `Zstd decompression failed: ${size} ${error} ${additional_data}`
	const copy = new Uint8Array(size)
	copy.set(new Uint8Array(wasm.memory.buffer, decompressed_addr, copy.byteLength))
	wasm.my_free(decompressed_addr)

	return copy
}

export function DecompressLZ4Chained(
	buf: Uint8Array,
	input_sizes: number[],
	output_sizes: number[],
): Uint8Array {
	if (input_sizes.length !== output_sizes.length)
		throw "Input and output count should match"
	const count = input_sizes.length
	let addr = wasm.my_malloc(buf.byteLength)
	new Uint8Array(wasm.memory.buffer, addr).set(buf)
	const inputs_addr = wasm.my_malloc(count * 4)
	new Uint32Array(wasm.memory.buffer, inputs_addr).set(input_sizes)
	const outputs_addr = wasm.my_malloc(count * 4)
	new Uint32Array(wasm.memory.buffer, outputs_addr).set(output_sizes)

	addr = wasm.DecompressLZ4Chained(addr, inputs_addr, outputs_addr, count)
	if (addr === 0)
		throw "LZ4 decompression failed"
	const copy = new Uint8Array(output_sizes.reduce((prev, cur) => prev + cur, 0))
	copy.set(new Uint8Array(wasm.memory.buffer, addr, copy.byteLength))
	wasm.my_free(addr)

	return copy
}

export function CloneWorldToProjection(mat: ArrayLike<number>): void {
	WASMIOBuffer.set(mat)
	wasm.CloneWorldToProjection()
}

export function WorldToScreenNew(
	position: Vector3,
	window_size: Vector2,
): Nullable<Vector2> {
	WASMIOBuffer[0] = position.x
	WASMIOBuffer[1] = position.y
	WASMIOBuffer[2] = position.z

	WASMIOBuffer[3] = window_size.x
	WASMIOBuffer[4] = window_size.y

	if (!wasm.WorldToScreenNew())
		return undefined
	return new Vector2(WASMIOBuffer[0], WASMIOBuffer[1])
}

export function DecompressVertexBuffer(
	buf: Uint8Array,
	elem_count: number,
	elem_size: number,
): Uint8Array {
	const buf_addr = wasm.my_malloc(buf.byteLength)
	if (buf_addr === 0)
		throw "Memory allocation for DecompressVertexBuffer raw data failed"
	new Uint8Array(wasm.memory.buffer, buf_addr, buf.byteLength).set(buf)

	const addr = wasm.DecompressVertexBuffer(buf_addr, buf.byteLength, elem_count, elem_size)
	const copy = new Uint8Array(elem_count * elem_size)
	copy.set(new Uint8Array(wasm.memory.buffer, addr, copy.byteLength))
	wasm.my_free(addr)

	return copy
}

export function DecompressIndexBuffer(
	buf: Uint8Array,
	elem_count: number,
	elem_size: number,
): Uint8Array {
	const buf_addr = wasm.my_malloc(buf.byteLength)
	if (buf_addr === 0)
		throw "Memory allocation for DecompressIndexBuffer raw data failed"
	new Uint8Array(wasm.memory.buffer, buf_addr, buf.byteLength).set(buf)

	const addr = wasm.DecompressIndexBuffer(buf_addr, buf.byteLength, elem_count, elem_size)
	const copy = new Uint8Array(elem_count * elem_size)
	copy.set(new Uint8Array(wasm.memory.buffer, addr, copy.byteLength))
	wasm.my_free(addr)

	return copy
}

export function ResetWorld(): void {
	wasm.ResetWorld()
}

export function LoadWorldModel(
	vertexBuffer: Uint8Array,
	vertexSize: number,
	indexBuffer: Uint8Array,
	indexSize: number,
	transform: ArrayLike<number>,
	flags: number,
): void {
	if (vertexSize < 3 * 4)
		return
	const vertexCount = vertexBuffer.byteLength / vertexSize
	const vertex_addr = wasm.my_malloc(vertexCount * 4 * 4)
	if (vertex_addr === 0)
		throw "Memory allocation for LoadWorldModel vertexBuffer raw data failed"
	if (flags !== -1 || vertexSize !== 4 * 4) {
		const wasmView = new DataView(wasm.memory.buffer, vertex_addr, vertexCount * 4 * 4),
			origView = new DataView(vertexBuffer.buffer, vertexBuffer.byteOffset, vertexBuffer.byteLength)
		for (let i = 0; i < vertexCount; i++) {
			const wasmPos = i * 4 * 4,
				origPos = i * vertexSize
			wasmView.setFloat32(wasmPos + 0, origView.getFloat32(origPos + 0, true), true)
			wasmView.setFloat32(wasmPos + 4, origView.getFloat32(origPos + 4, true), true)
			wasmView.setFloat32(wasmPos + 8, origView.getFloat32(origPos + 8, true), true)
			wasmView.setUint32(wasmPos + 12, flags, true)
		}
	} else
		new Uint8Array(wasm.memory.buffer, vertex_addr, vertexBuffer.byteLength).set(vertexBuffer)

	const index_addr = wasm.my_malloc(indexBuffer.byteLength)
	if (index_addr === 0)
		throw "Memory allocation for LoadWorldModel indexBuffer raw data failed"
	new Uint8Array(wasm.memory.buffer, index_addr, indexBuffer.byteLength).set(indexBuffer)

	WASMIOBuffer.set(transform)
	wasm.LoadWorldModel(
		vertex_addr, vertexCount * 4 * 4,
		index_addr, indexBuffer.byteLength, indexSize,
	)
}

export function FinishWorld(): void {
	wasm.FinishWorld()
}

export function FinishWorldCached(cached_bvh: [Uint8Array, Uint8Array]): void {
	const cached_bvh1_addr = wasm.my_malloc(cached_bvh[0].byteLength)
	if (cached_bvh1_addr === 0)
		throw "Memory allocation for FinishWorldCached cached_bvh[0] raw data failed"
	new Uint8Array(wasm.memory.buffer, cached_bvh1_addr, cached_bvh[0].byteLength).set(cached_bvh[0])

	const cached_bvh2_addr = wasm.my_malloc(cached_bvh[1].byteLength)
	if (cached_bvh2_addr === 0)
		throw "Memory allocation for FinishWorldCached cached_bvh[1] raw data failed"
	new Uint8Array(wasm.memory.buffer, cached_bvh2_addr, cached_bvh[1].byteLength).set(cached_bvh[1])

	wasm.FinishWorldCached(
		cached_bvh1_addr, cached_bvh[0].byteLength,
		cached_bvh2_addr, cached_bvh[1].byteLength,
	)
}

export function ExtractWorld(): [Uint8Array, Uint8Array, Uint8Array, Uint8Array] {
	wasm.ExtractWorld()
	return [
		new Uint8Array(wasm.memory.buffer, WASMIOBufferU32[0], WASMIOBufferU32[1]),
		new Uint8Array(wasm.memory.buffer, WASMIOBufferU32[2], WASMIOBufferU32[3]),
		new Uint8Array(wasm.memory.buffer, WASMIOBufferU32[4], WASMIOBufferU32[5]),
		new Uint8Array(wasm.memory.buffer, WASMIOBufferU32[6], WASMIOBufferU32[7]),
	]
}
